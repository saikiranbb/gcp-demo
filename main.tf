module "vpc" {
    source  = "github.com/terraform-google-modules/terraform-google-network"

    project_id   = "jenkins-314106"
    network_name = "jenkins2-vpc"
    routing_mode = "GLOBAL"

    subnets = [
        {
            subnet_name           = "subnet1"
            subnet_ip             = "10.244.64.0/21"
            subnet_region         = "us-east4"
        },
        {
            subnet_name           = "subnet2"
            subnet_ip             = "10.244.72.0/21"
            subnet_region         = "us-east4"
            subnet_private_access = "true"
            subnet_flow_logs      = "true"
            description           = "This subnet has a description"
        }
]
}
